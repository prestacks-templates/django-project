"""
Django middleware functionality for the $NAME project.
"""
import logging

from django.conf import settings
from django.http import HttpRequest, HttpResponse
from django.utils.deprecation import MiddlewareMixin
from django.views.debug import ExceptionReporter
from django.views.defaults import server_error

logger = logging.getLogger("django")


class HealthCheckMiddleware(MiddlewareMixin):
    """
    Used for the Load Balancer to confirm the server is running.
    """

    def process_request(self, request):  # pylint: disable=R0201
        """
        Called for every check, and just checks if we're looking at the /_health/ path.
        """
        if request.META["PATH_INFO"] == "/_health/":
            return HttpResponse("pong")
        return None


class StackTraceMiddleware(MiddlewareMixin):
    """
    Formats the stack trace for any exception raised to replace newlines with carriage returns.

    This is so AWS CloudWatch logs are in a single log, rather then across multiple lines.
    """

    def process_exception(self, request: HttpRequest, exception: Exception):  # pylint: disable=R0201
        """
        Called whenever an unhandled exception is raised in a view.

        Intercepts the stacktrace and uses our custom logger so we can use a custom formatter.

        Args:
            request: The request which invoked the view causing the error.
            exception: The exception raised in the view.
        """
        import traceback  # pylint: disable=C0415

        logger.error(traceback.format_exc())

        if settings.DEBUG:
            reporter = ExceptionReporter(request, type(exception), exception, exception.__traceback__)
            return HttpResponse(reporter.get_traceback_html())
        return server_error(request)
