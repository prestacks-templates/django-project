#!/bin/bash
python $NAME/manage.py collectstatic
(cd $NAME; gunicorn -b 0.0.0.0:8001 --enable-stdio-inheritance --timeout 300 --worker-class gevent --capture-output --threads=4 --chdir $NAME/ wsgi:application --daemon)
nginx
