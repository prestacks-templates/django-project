FROM python:3.9

ARG ENV_NAME
ENV PRESTACKS_ENV=$ENV_NAME

COPY requirements.txt /code/
WORKDIR /code
RUN pip3 install -r requirements.txt
RUN apt update; apt install nginx -y

COPY nginx.conf /etc/nginx/
COPY . /code/

RUN ["chmod", "+x", "/code/scripts/start_server.sh"]

EXPOSE 8000

CMD [ "/code/scripts/start_server.sh" ]
